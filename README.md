# anydoor

Tiny NodeJS Static Web Server

#install
```
npm i -g anydoor
```

#usage
```
anydoor # set current directory as the root directory on the static web Server

anydoor -p 8080 # set port as 8080

anydoor -h localhost # set host as localhost

anydoor -d /usr # set the root directory as /usr
```